<?php

declare(strict_types=1);

namespace App\Presenters;

use UserMestak;
use Nette;

final class HomepagePresenter extends Nette\Application\UI\Presenter {

    public function actionDefault() {

        $pin1 = new \models\UtilsMichalMestak();

        echo $pin1->getPinMichalMestak();
        echo '<br>';
        echo $pin1->getPinMichalMestak();
        echo '<br>';
        echo $pin1->getPinMichalMestak();
        echo '<br>';
        echo $pin1->getPinMichalMestak();
        echo '<br>';
        echo $pin1->getPinMichalMestak();
        echo '<br>';
        echo '<br>';

        $Mike = new UserMestak();
        $Mike->setUsernameMestak("Mike");

        echo $Mike->getStreetMestak();
        echo $Mike->getZipMestak();
        echo $Mike->getInvoiceIdMestak();
        echo $Mike->getLoginCountMestak();
        echo $Mike->getBornDateMestak();

        dump($Mike);
    }

}
